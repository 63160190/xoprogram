package com.mycompany.xoprogram;


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class xoprogram {

    static Scanner kb = new Scanner(System.in);
    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish=false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();
        while(true){
        showTable(table);
        showTurn();
        inputRowCol();
        process();
        if(finish){
            break;
        }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public static void showTable(char[][] table1) {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn" + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,col");
        row = kb.nextInt();
        col = kb.nextInt();
    }
    

    public static void process() {
        if(setTable()){
            if(checkWin()){
                finish=true;
                showTable(table);
                System.out.println(">>>"+currentPlayer+"<<<");
                return;
            }
            if(checkDraw()){
                finish=true;
        
                return;
            }
           switchPlayer();
        }

         
  
    }
    public static void switchPlayer(){
       if(currentPlayer=='O'){
           currentPlayer='X';    
       }else{
          currentPlayer='O';
       }
    }

    public static boolean setTable(){
         table[row-1][col-1]=currentPlayer;//setTable(row,col)
         return true;
    }
    public static boolean checkWin() {
        if(checkVertical()){
            return true;
        }else if(checkHorizontal()){
            return true;        
        }else if(checkX()){
            return true;
        }
        return false;
    }

    public static boolean checkVertical() {
        for(int r=0;r<table.length;r++){
            if(table[r][col-1]!=currentPlayer)return false;
            
        }
        return true;
    }

    public static boolean checkHorizontal() {
       for(int c=0;c<table.length;c++){
            if(table[c][col-1]!=currentPlayer)return false;
            
        }
        return true;
    }

   public static boolean checkX(char[][] table, char currentPlayer) {
        if (checkX1(table,currentPlayer)) {
            return true;
        } else if (checkX2(table,currentPlayer)) {
            return true;
        }
        return false;
    }


     public static boolean checkX1(char table[][], char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char table[][], char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;

    }

    private static boolean checkX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Object checkHorizontal(char[][] table, char currentPlayer, int row) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Object checkVertical(char[][] table, char currentPlayer, int col) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Object checkDraw(int count) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Object checkWin(char[][] table, char currentPlayer, int col, int row) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
   
   